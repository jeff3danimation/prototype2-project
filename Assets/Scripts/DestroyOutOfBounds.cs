﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOutOfBounds : MonoBehaviour
{
    private float topBounds = 30;
    private float lowBounds = -10;

    // Update is called once per frame
    void Update()
    {
        if (transform.position.z > topBounds || transform.position.z < lowBounds)
        {
            if (transform.position.z < lowBounds)
            {
                Debug.Log("GAME OVER MAN, GAME OVER!");
            }
            Destroy(gameObject);
        }
    }
}
